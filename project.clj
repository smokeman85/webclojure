(defproject webclojure "0.1.1"
            :description "Web editor for clojure"
            :dependencies [[org.clojure/clojure "1.4.0"]
                           [noir "1.3.0-beta3"]
                           [enlive "1.1.1"]
                           [clojail "1.0.6"]]
            :main webclojure.server
            :plugins [[lein-swank "1.4.5"]])

