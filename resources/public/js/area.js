function TextArea(selector){
    this.selector = selector;
}

TextArea.prototype.addText=function(text){
    $(this.selector).val(text);
}

TextArea.prototype.addNewResult = function(result, out){
    var value = $(this.selector).val();
    $(this.selector).val(value + "\n" + result + " " + out);
}


TextArea.prototype.addInputChar = function(){
    var value = $(this.selector).val();
    $(this.selector).val(value + ">");
}

TextArea.prototype.deleteLineFeed = function(string){
    return string.replace(/[\r\n]/g, ' ');
}

TextArea.prototype.delInputLine = function(line){
    return line.replace(">","");
}

TextArea.prototype.getInputLine = function(){
    return getLine($(this.selector)[0]).content.replace(">","");
}

TextArea.prototype.getCurrentLine = function(){
    return getLine($(this.selector)[0]).content;
}

TextArea.prototype.getText = function(){
    return $(this.selector).val();
}

TextArea.prototype.getLineNumber = function(number){
}

TextArea.prototype.getCountLine = function(){
    var text = $(this.selector).val();   
    var lines = text.split(/\r|\r\n|\n/);
    var count = lines.length;
    
    return count;
}



function getLine(edit){
    var start = 0;
    var lineNumber = 0;
    var content = "";
    if(typeof edit.selectionStart == 'undefined') {return false}
    
    start = edit.selectionStart;
    
    lineNumber = edit.value.substr(0,start).split("\n").length - 1;
    content = edit.value.split("\n")[lineNumber];
    
    return {"lineNumber": lineNumber, "content": content}
}


var repl = new TextArea("#ar");
var code = new TextArea("#ac");

function Success(area, data){
    var obj = $.parseJSON(data);
    if(obj.error)
	area.addNewResult(obj.message, " ");
    else
	area.addNewResult(obj.result, obj.out);
    $('#ar').scrollTop(99999);
}


function Compile(){
    var code = new TextArea("#ac");
    var countline = code.getCountLine();
    var stringArray = $("#ac").val().split('\n');
    var codeline = "";
    var i;
    for(i = 0; i < countline; i++){
	codeline += stringArray[i];
	if(isBalanced(codeline)){
	    
	
	    $.ajax({
		type: "POST",
		url: '/repl',
		dataType: 'text',
		async: false,
		data: {"form": codeline},
		success:function(data){
		    Success(repl, data);
		}
	    })
	    codeline = "";
	}
    }
}

function initRepl(){
    var line = "";

    $('#ar').keypress(function(event) { 
	if (event.keyCode == 13) {
	    line = line + repl.getCurrentLine();
	    if(isBalanced(line)){
		rhistory.Add(line);
		$.ajax({
		    type: "POST",
		    url: '/repl',
		    dataType: 'text',
		    async: false,
		    data: {"form": repl.delInputLine(line)},
		    success:function(data){
			Success(repl, data);
		    }
		})
		line = "";
		$('#ar').scrollTop(99999);
	    }
	}


    });
    
    $('#ar').keyup(function(event) { 
	if (event.keyCode == 13) {
	    if(isBalanced(line)){
		repl.addInputChar();
		$('#ar').scrollTop(99999);
	    }
	}
    });

}