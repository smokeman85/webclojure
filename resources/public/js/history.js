function ReplHistory(){
    this.history = new Array();
}

ReplHistory.prototype.Add = function(text){
    this.history.push(text);
}

ReplHistory.prototype.Count = function(){
    return this.history.length;
}

ReplHistory.prototype.Clear = function(){
    this.history.length = 0;
}

ReplHistory.prototype.Get = function(i){
    return this.history[i];
}

rhistory = new ReplHistory();
