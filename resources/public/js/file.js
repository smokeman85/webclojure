
function CreateLoadFileList(list, filediv){
    $(filediv).append('<ul id="listfile" />');
    $.each(list, function(i){
        $('#listfile').append('<li id= '+list[i]+'>'+ list[i]+ '</li>');
    });

    $('ul li').click(function(){
	LoadFile(this.id);
	$('ul').remove();
	$('#cancelbutton').remove();
    });
    $(filediv).append('<button tupe="button" id="cancelbutton">cancel</button>');
    $('#cancelbutton').click(function(){
	$('ul').remove();
	$('#cancelbutton').remove();
    });
}

function CreateSaveFileList(list, filediv){
    
    $(filediv).append('<ul id="listfile" />');
    $.each(list, function(i){
        $('#listfile').append('<li id= '+list[i]+'>'+ list[i]+ '</li>');
    });
    $(filediv).append('<input type="text" id="filename" size="40">');
    $('ul li').click(function(){
	$('#filename').val(this.id);
    });
    $(filediv).append('<button tupe="button" id="savebutton">save</button>');
    $('#savebutton').click(function(){
	SaveFile($('#filename').val(), $('#ac').val());
	$('ul').remove();
	$('#savebutton').remove();
	$('#cancelbutton').remove();
	$('#filename').remove();
    });
    $(filediv).append('<button tupe="button" id="cancelbutton">cancel</button>');
    $('#cancelbutton').click(function(){
	$('ul').remove();
	$('#cancelbutton').remove();
	$('#savebutton').remove();
	$('#filename').remove();
    });

}

function SaveFile(filepath, data){
    $.ajax({
	type: "POST",
	url: '/savefile',
	dataType: 'text',
	async: false,
	data:{"filepath": filepath, "data" : data},
	success:function(){
	    alert("File saved");
	}
    })
}

function LoadFile(filepath){
    $.ajax({
	type: "POST",
	url: '/loadfile',
	dataType: 'text',
	async: false,
	data:{"filepath": filepath},
	success:function(data){
	    var obj = $.parseJSON(data);
	    code.addText(obj.filedata);
	}
    })
}

function SetFile(){
    $.ajax({
	type: "POST",
	url: '/filelist',
	dataType: 'text',
	async: false,
	success:function(data){
	    var obj = $.parseJSON(data);
	    CreateSaveFileList(obj.filelist, '#FileList');
	}
    })
}

function GetFile(){
    $.ajax({
	type: "POST",
	url: '/filelist',
	dataType: 'text',
	async: false,
	success:function(data){
	    var obj = $.parseJSON(data);
	    CreateLoadFileList(obj.filelist, '#FileList');
	}
    })
}