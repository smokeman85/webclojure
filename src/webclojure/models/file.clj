(ns webclojure.models.file
  (:use [clojure.java.io]))

(def file-path "/home/builder/test")

(defn #^String substring?                                                                                                                 
  [substring #^String s]                                                                                                                            
  (.contains s substring))

(defn get-file-dir-list [dir]
  (sort (map #(.getCanonicalPath %) (.listFiles (clojure.java.io/file dir)))))

(defn get-file-list []
  (get-file-dir-list file-path))

(defn get-file-list-request []
  {:filelist (get-file-list)})

(defn read-file [file]
  (slurp file))

(defn read-file-request [file]
  {:filedata (read-file file)})


(defn write-file [file data]
  (spit file data))

(defn write-file-request [file data]
  (if (substring? file-path file)
    (write-file file data)))


