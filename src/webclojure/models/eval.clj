(ns webclojure.models.eval
  (:use [clojail.core :only [sandbox]]
        [clojail.testers :only [blacklist-symbols blacklist-objects]]
        [clojure.stacktrace :refer [root-cause]])
  (:import java.io.StringWriter))

(def tester [(blacklist-objects [java.lang.Thread])])

(def sb (sandbox tester))

(defn eval-form [form]
  (with-open [out (StringWriter.)]
    (let [result (sb form {#'*out* out})]
       {:expr form
       :result (str result) 
        :out (str out)})))

(defn eval-string [expr]
  (let [form (binding [*read-eval* false] (read-string expr))]
    (eval-form form)))

(defn eval-request [expr]
  (try
    (eval-string expr)
    (catch Exception e
      {:error true :message (str (root-cause e))})))