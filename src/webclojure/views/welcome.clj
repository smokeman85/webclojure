(ns webclojure.views.welcome
  (:require [webclojure.views.common :as common])
  (:use [noir.core :only [defpage]])
  (:use [noir.response :only [json]])
  (:use [webclojure.models.eval :only [eval-request]])
  (:use [webclojure.models.file :only [get-file-list-request read-file-request write-file-request]]))

(defpage "/" []
         (common/index))

(defpage [:post "/repl"] {:keys [form]} 
  (json (eval-request form)))

(defpage [:post "/filelist"] [] 
  (json (get-file-list-request)))

(defpage [:post "/loadfile"] {:keys [filepath]}
  (json (read-file-request filepath)))

(defpage [:post "/savefile"] {:keys [filepath data]}
  (write-file-request filepath data))


