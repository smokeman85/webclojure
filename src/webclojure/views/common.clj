(ns webclojure.views.common
  (:use [noir.core :only [defpartial]]
        [hiccup.page :only [include-css html5]]
        [net.cgrand.enlive-html :as enlive]))

(defpartial layout [& content]
            (html5
              [:head
               [:title "webclojure"]
               (include-css "/css/reset.css")]
              [:body
               [:div#wrapper
                content]]))

(enlive/deftemplate index "webclojure/views/html/index.html" [])